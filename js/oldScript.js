$(document).ready(function(){ // $ est un alias a la fonction jQuery
	// A connaitre pour le controle 
	var imgg = $("img");

	$("button:eq(0)").click(function(){
		imgg.hide();	
	});

	$("button:eq(1)").click(function(){
		imgg.show();	
	});

	$("button:eq(2)").click(function(){
		imgg.toggle();	
	}); 
	
	
	$("form").submit(function(){

		$(this).find(".error").remove();
		$(this).find("*").css("outline","none");

		var nom = $(this).find("[type='text']");
		var comment = $(this).find("textarea");
		var cgu = $(this).find("[type='checkbox']");
		var error = false;

		if( nom.val().trim().length == 0 )
		{
			nom.after("<p class='error'> Erreur sur le nom</p>");
			nom.css("outline","1px solid red");
			// nom.addClass("csserror"); pour rajouter la classe déjà présente dans la feuille de style qui va personnalisé le input
			error = true;
		}

		if( comment.val().trim().length == 0 )
		{
			comment.after("<p class='error'> Erreur sur le commentaire</p>");
			comment.css("outline","1px solid red");
			error = true;	
		}

		// if( cgu.is(":checked") == false ) fonctionne mais lourd car il va regarder tous les éléments cochés 

		if( !cgu.is(":checked") ) // ou !cgu.prop("checked")
		{
			cgu.after("<p class='error'> Erreur sur les cgu</p>");
			cgu.css("outline","1px solid red");
			error = true;
		}

		return !error;
	})

	$("img").hover(
		function(){ $(this).attr("src", $(this).attr("srcnew") ) },
		function(){ $(this).attr("src", $(this).attr("srcold") ) }
	);

	/* $(img).mouseover(function(){
		$( this ).attr( "src", "img2.jpg");

		$(img).mouseout(function(){
			$( this ).attr( "src", "tracer.jpg");	
		})
	})*/

	











});