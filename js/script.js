$(document).ready(function(){

	var interval = 0;
	var lock = false; 

	var wait = 3000; // temps d'affichage d'une image
	var transition = 1000; // temps de transitions des images

	play(); // Lance automatiquement la fonction play() qui va faire défiler automatiquement les images

	// objet littéral à changer pour récupérer les données en base
	var slides = [
		{src: "img/img1.jpg", desc: "Un paysage"},
		{src: "img/img2.jpg", desc: "Le panda de Google"},
		{src: "img/img3.jpg", desc: "Des fruits"},
		{src: "img/img4.jpg", desc: "Du bois et une feuille"}
	];

	for(slide in slides){ // Fais apparaitre autant d'image qu'il trouve dans l'objet littéral
		$("#slide").append("<div class='element'><img src='" + slides[slide].src + "'/> <p class='desc'>"+slides[slide].desc+"</p></div> ");	
	}
	
	$("#slideshow").hover(function(){ // Le carousel se met en pause dès que la souris passe au dessus 
 		if(interval != 0){
 			clearInterval(interval); // Clear interval permet de stopper le timing entre les slide : met en pause le carousel 
 			interval = 0;
 		}	
  	},function(){ // Callback : et se remet en play lorsque la souris repart 
  		play();
	});

	 $("#slideshow").mouseover(function(){ // permet d'afficher les boutons play before et next lorsque la souris passe sur le carousel
	 	
	  	$("#before").css("display", "block");
  		$("#next").css("display", "block");
  		$("#play").css("display", "block");
		
	 	$("#slideshow").mouseout(function(){
	 		$("#before").css("display", "none");
	  		$("#next").css("display", "none");
	  		$("#play").css("display", "none");
	 	})
	 })

	function next(){ // Affiche le slide suivant (et sert à la fonction play)
		if(lock){ // Permet de "bloquer" la fonction jusqu'à la fin de l'animation
			return;
		}
		
		lock = true;
		
		$("#slide").animate({
			marginLeft : "-500px"
		}, transition,
		function(){
			$("#slide .element:last").after($("#slide .element:first"));
			$("#slide").css("margin-left", "0px");
			lock = false; 
		});
	}

	function before(){ // affiche le slide précedent 
		if(lock){
			return;
		}
		
		lock = true;
		
		$("#slide").css("margin-left", "-500px"); // le slide passe à 500px à gauche 
		$("#slide .element:first").before($("#slide .element:last")); // le dernier slide se positionne avant le premier slide 
		$("#slide").animate({ // et l'animation donne l'effet d'aller vers la gauche (on était à -500px on va à 0px)
			marginLeft : "0px" 
		}, transition,
		function(){
			$("#slide").css("margin-left", "0px"); // On remet la bonne valeur à la marge pour que le slide ne bouge pas a la fin de l'animation
			lock = false;
		});
	}

	function play(){ // Lance le carousel de façon à ce que les slides tournent automatiquement
		if(interval != 0){
			clearInterval(interval);
			interval = 0;
			$("#play").attr("src", "img/play.png");
			return;
		}

		interval = setInterval(function(){ next(); }, wait); // set interval prend en argument la fonction que l'on souhaite lancé, et la durée entre chaque répétition, ici c'est next qui sera répété et wait la valeur de l'attente en miliseconde (voir au début de la page)
		$("#play").attr("src", "img/pause.png"); // On change l'icone lorsque le play est lancé pour afficher le boutton pause 
	}

	// Ce qui suit s'active lorsqu'un clique est effectué sur un des boutons  
	$("#next").click(function(){ // la fleche suivante lance next()
		clearInterval(interval); // met pause au carousel
		next();
	});

	$("#before").click(function(){ // la fleche precedente lance before()
		clearInterval(interval);
		before();
	});

	$("#play").click(play); // Le bouton play lance la play() -> qui change aussi l'icone

	$(document).keydown(function(e) { // lorsque le focus est sur le document, 2 touches ont été assignés à des fonctions : e est la touche la valeur de la touche dans ce switch
    	switch(e.which) { 
	        case 37: // c'est la fleche gauche, elle lance before()
	        clearInterval(interval);
	        before(); 
	        break;

	        case 39: // fleche droite qui lance next()
	        clearInterval(interval);
	        next(); 
	        break;

	        default: return; // Si il ne s'agit pas d'une de ces deux touches, le switch se stop
    	}
    e.preventDefault(); // Cette fonction permet de stabiliser le switch au cas ou plusieurs touches sont tapés afin d'en traiter qu'une à la fois
	});
});
